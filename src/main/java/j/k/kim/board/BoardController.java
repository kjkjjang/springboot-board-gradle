package j.k.kim.board;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/board")
public class BoardController {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@RequestMapping(method=RequestMethod.GET, value="/writePage")
	public String writeGetPage() {
		return "board/write";
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/writePage")
	public String writePostPage() {
		
		return "board/write";
	}
	
	@RequestMapping({"/listPage/{pageIndex}", "/listPage"})
	public String listPage(@PathVariable Optional<String> pageIndex) {
		int index = -1;
		if (pageIndex.isPresent()) {
			try {
				String value = pageIndex.get();
				index = Integer.parseInt(value);
			} catch (NumberFormatException e) {
			}
		} else {
			// last page
		}
		logger.info("index[" + index + "]");
		return "board/listPage";
	}
	
	@RequestMapping({"/showItem/{itemIndex}", "/showItem"})
	public String showItem(@PathVariable Optional<String> itemIndex) {
		long index = -1;
		if (itemIndex.isPresent()) {
			try {
			String value = itemIndex.get();
			index = Long.parseLong(value);
			} catch(NumberFormatException e) {
			}
		} else {
			// last page
		}
		logger.info("index[" + index + "]");		
		return "board/showItem";
	}
}
