package j.k.kim.config;

import javax.sql.DataSource;

import org.h2.server.web.WebServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class DataSourceConfig {
	
	@Bean
	public PlatformTransactionManager transactionManager(DataSource dataSource) {
		DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
		transactionManager.setDataSource(dataSource);
		return transactionManager;
	}
	
	@Bean
	public JdbcTemplate jdbcTemplate(DataSource dataSource) {
		return new JdbcTemplate(dataSource);
	}
	
	@Bean
	@Profile("test")
	public DataSource getEmbeddedDataSource() {
		return new EmbeddedDatabaseBuilder()
			.setType(EmbeddedDatabaseType.H2)
			.addScript("classpath:j/k/kim/schema.sql")
			.build();
	}

	@Bean
	@Profile("prod")
	public DataSource getMySqlDataSource() {
		/*
		  <bean id="dataSource" class="org.springframework.jdbc.datasource.SimpleDriverDataSource" >
		  <property name="driverClass" value="com.mysql.jdbc.Driver" />
		  <property name="url" value="jdbc:mysql://localhost:3306/test?characterEncoding=UTF-8" />
		  <property name="username" value="root" />
		  <property name="password" value="mysql" />
		  </bean>
		 */
		return new SimpleDriverDataSource(
				// driver, url, user, password)
				new org.mariadb.jdbc.Driver(), 
				"jdbc:mariadb://wendies.org:3306/kjk850",  
				"kjk850", "sezgov6k2");
	}
	
	/**
	 * 	h2 console 
	 */
	@Bean
	public ServletRegistrationBean h2servletRegistration() {
	    ServletRegistrationBean registration = new ServletRegistrationBean(new WebServlet());
	    registration.addUrlMappings("/h2/*");
	    return registration;
	}
}
