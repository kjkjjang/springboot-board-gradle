package j.k.kim.config;

import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseStatus;

import j.k.kim.home.PageNotFoundException;
import j.k.kim.home.ServerErrorException;

@ControllerAdvice
public class GlobalAttributeAdvice {

	@ModelAttribute(name = "jqueryHome")
	public String jqueryHome() {
		return "/resources/jquery";
	}

	@ModelAttribute(name = "bootstrapHome")
	public String bootstrapHome() {
		return "/resources/bootstrap-3.3.7-dist";
	}

	@ModelAttribute(name="summernoteHome")
	public String summernoteHome() {
		return "/resources/summernote";
	}
	
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(value = PageNotFoundException.class)
	public String pageNotFoundException(PageNotFoundException e, Model model) {
		model.addAttribute("exception", e);
		return "error/error";
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(value = ServerErrorException.class)
	public String serverErrorException(ServerErrorException e, Model model) {
		model.addAttribute("exception", e);
		return "error/error";
	}

}
