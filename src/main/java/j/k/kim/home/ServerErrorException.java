package j.k.kim.home;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR)
public class ServerErrorException extends RuntimeException {

	private static final long serialVersionUID = -7701855097727586651L;

	private String errorMessage;

	public ServerErrorException() {
		this("server internal error");
	}

	public ServerErrorException(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getMessage() {
		return errorMessage;
	}

}
