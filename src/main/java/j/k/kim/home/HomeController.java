package j.k.kim.home;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@RequestMapping("/")
	public String indexPage() {
		logger.info("index page called");
		return "index";
	}
	
	@RequestMapping("/404")
	public String pageNotFound() {
		throw new PageNotFoundException();
	}
	
	@RequestMapping("/500")
	public String serverError() {
		throw new ServerErrorException();
	}
}
