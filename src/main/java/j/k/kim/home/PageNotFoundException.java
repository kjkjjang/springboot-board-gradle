package j.k.kim.home;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="page not found", code=HttpStatus.NOT_FOUND)
public class PageNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -7701855097727586651L;

	private String errorMessage;

	public PageNotFoundException() {
		this("page not found");
	}

	public PageNotFoundException(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getMessage() {
		return errorMessage;
	}
	

}
