package j.k.kim.map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/map")
public class MapController {

	@RequestMapping
	public String defaultMapPage() {
		return "map/index";
	}
	
	
}
