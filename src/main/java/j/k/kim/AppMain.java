package j.k.kim;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class AppMain {
	
	private static Logger logger = LoggerFactory.getLogger(AppMain.class);
	
	public static void main(String[] args) {
		logger.info("spring startup");
		SpringApplication.run(AppMain.class, args);
	}
	
	public static SpringApplicationBuilder getSpringApplicationBuilder() throws UnknownHostException {
		String hostname = InetAddress.getLocalHost().getHostName();
		System.out.println("hostname[" + hostname + "]");
		if (hostname.equals("wendies.org")) {
			return getRealProfile();
		} else {
			String ip = InetAddress.getLocalHost().getHostAddress();
			System.out.println("ip[" + ip + "]");
			if (ip.startsWith("180.71.58.87")) {
				return getRealProfile();
			} else {
				return getTestProfile();
			}
		}
	}
	
	public static SpringApplicationBuilder getRealProfile() {
		return new SpringApplicationBuilder(AppMain.class)
		.profiles("real")
		.properties("server.port=9999");
	}
	
	public static SpringApplicationBuilder getTestProfile() {
		return new SpringApplicationBuilder(AppMain.class)
		.profiles("test")
		.properties("server.port=9999");
	}
}
