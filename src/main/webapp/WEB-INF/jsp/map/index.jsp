<%@ page contentType="text/html; charset=EUC-KR"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
<title>지도</title>
<link href="${bootstrapHome}/css/bootstrap.min.css" rel="stylesheet">
<script src="${jqueryHome}/jquery-3.3.1.js"></script>
<script src="${bootstrapHome}/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://openapi.map.naver.com/openapi/v3/maps.js?clientId=2SovBoxNDDfQeltbW4pT"></script>
<style type="text/css">
/* need for div element height 100% */
html, body {
	height: 100%;
}
</style>
</head>
<body>
	<!-- <div id="map" style="width:100%; height:100%;"></div> -->
	<div id="map" style="float:left; width:100%;height:400px"></div>
	<div id="user-saved-map" style="float:left;">
		<ul>
			<li>저장목록1</li>
			<li>저장목록2</li>
			<li>저장목록3</li>
			<li>저장목록4</li>
		</ul>
	</div>
<form action="/MapController/saveMap">
	<table id="map-info" border="1" style="clear: both;">
		<tr>
			<th width="30">순번</th>
			<th width="200">내용</th>
			<th width="150">위도</th>
			<th width="150">경도</th>
			<th width="80">옵션</th>
		</tr>
	</table>
	<button type="submit">저장</button>
</form>
<p style="clear: left;">맵 정보를 입력하여 주세요</p>


	<script>
	Array.prototype.remove = function(from, to) {
		  var rest = this.slice((to || from) + 1 || this.length);
		  this.length = from < 0 ? this.length + from : from;
		  return this.push.apply(this, rest);
		};

	// polyline
	var poly;
	// marker
	var markersArray = [];

	var divIdArray = [];
	var divId = 0;

	// for remove item
	var clickedMarker;
	var clickedInfoWindow;

	var isInfoWindowOpened=false;

	
		var map = new naver.maps.Map('map', {
			zoom : 8,
			center : new naver.maps.LatLng(37.3614483, 127.1114883)
		});

		var menuLayer = $('<div style="position:absolute;z-index:10000;background-color:#fff;border:solid 1px #333;padding:10px;display:none;"></div>');


		map.getPanes().floatPane.appendChild(menuLayer[0]);

		// 화살표
		poly = new naver.maps.Polyline({
			path : [],
			map : map,
			endIcon : naver.maps.PointingIcon.OPEN_ARROW,
			//아이콘의 크기를 지정합니다. 단위는 픽셀입니다.
			endIconSize : 20,
			strokeColor : '#ff0000',
			strokeWeight : 5
		});

		naver.maps.Event.addListener(map, 'click', addLatLng);

		function addLatLng(event) {
			var clickPoint = event.coord;
			if (isInfoWindowOpened) {
				clickedInfoWindow.close();
				isInfoWindowOpened = false;
			} else {
				var path = poly.getPath();
				path.push(clickPoint);


				var marker = new naver.maps.Marker({
					position : clickPoint,
					title : '' + path.getLength(),
					map : map
				});

				markersArray.push(marker);
				attachMarkerInfo(marker);
				appendInfoLine();
			}
		}
		
		function attachMarkerInfo(marker) {
			divIdArray.push("div" + divId);
			var infoWindowContent = '<button onclick="removeMarker();">삭제</button>'
					+ '<div class="map-info-window" contenteditable="true" placeholder="click here to edit" ' 
					+ 'id="div' + divId + '"></div>';
			divId++;
			var infowindow = new naver.maps.InfoWindow({
				content : infoWindowContent
			});
			
			naver.maps.Event.addListener(infowindow, 'closeclick', function() {
				var divId = 'div#' + divIdArray[getClickedIndex()];
				var content = clickedInfoWindow.getContent();
				content = content.substring(0, content.length - 6) + $(divId).text() + '</div>';
				clickedInfoWindow.setContent(content);
			});
			
			naver.maps.Event.addListener(marker, 'click', function() {
				//infowindow.open(marker.get('map'), marker);
				if (!isInfoWindowOpened) {
					infowindow.open(map, marker);
					clickedMarker = marker;
					clickedInfoWindow = infowindow;
				} else {
					clickedInfoWindow.close();
				}
				isInfoWindowOpened = !isInfoWindowOpened;
			});
		}
		
		function appendInfoLine() {
			var number = markersArray.length;
			var lastMarker = markersArray[number - 1];
			var latitudeLongitude = lastMarker.getPosition();

			var content = '<tr>'
					+ '<td><input type="text" id="sequence" name="sequence" value="' + number + '"></td>'
					+ '<td><input type="text" id="content" name="content"></td>'
					+ '<td><input type="text" id="latitude" name="latitude" value="'
					+ latitudeLongitude.lat()
					+ '"></td>'
					+ '<td><input type="text" id="longitude" name="longitude" value="'
					+ latitudeLongitude.lng() + '"></td>' + '<td>수정,삭제</td>'
					+ '</tr>';
			$('#map-info').append(content);
		}

		function removeMarker(marker) {
			clickedInfoWindow.close();
			clickedMarker.setMap(null);
			var removeItemIndex = getClickedIndex();
			if (removeItemIndex != -1) {
				markersArray.remove(removeItemIndex);
				divIdArray.remove(removeItemIndex);
				poly.getPath().removeAt(removeItemIndex);
				isInfoWindowOpened = false;
			}
		}

		function getClickedIndex() {
			for (i = 0; i < markersArray.length; i++) {
				if (markersArray[i] == clickedMarker) {
					return i;
				}
			}
			return -1;
		}
		
	</script>
</body>
</html>