<%@ page contentType="text/html; charset=EUC-KR"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>write page</title>

<link href="${bootstrapHome}/css/bootstrap.min.css" rel="stylesheet">
<script src="${jqueryHome}/jquery-3.3.1.js"></script>
<script src="${bootstrapHome}/js/bootstrap.min.js"></script>

<!-- include summernote css/js-->
<link href="${summernoteHome}/summernote.css" rel="stylesheet">
<script src="${summernoteHome}/summernote.js"></script>

<script>
	$(document).ready(function() {
		$('#summernote').summernote({
			lang: 'ko-KR',
			height : 300,
			minHeight : null,
			maxHeight : null,
			focus : true
		});
	});
	
	function getHtmlCode() {
		var markupStr = $('#summernote').summernote('code');
		return markupStr;
	}
	
	function setHtmlCode() {
		var markupStr = 'hello world';
		$('#summernote').summernote('code', markupStr);
	}
</script>

</head>

<body>
	<form>
		<div class="container">
			<label for="title">글 제목</label>
			<div class="form-group">
				<input type="text" class="form-control" id="title" placeholder="title">
			</div>
			<label for="body">글 내용</label>
			<div id="summernote"></div>
			<button type="submit" class="btn btn-default">Submit</button>
		</div>
	</form>
</body>
</html>
