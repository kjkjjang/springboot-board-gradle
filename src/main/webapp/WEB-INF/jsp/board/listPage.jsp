<%@ page contentType="text/html; charset=EUC-KR"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>list page</title>

<link href="${bootstrapHome}/css/bootstrap.min.css" rel="stylesheet">
<script src="${jqueryHome}/jquery-3.3.1.js"></script>
<script src="${bootstrapHome}/js/bootstrap.min.js"></script>

</head>

<body>
	<div class="container">

		<div class="panel panel-default">
			<!-- Default panel contents -->
			<div class="panel-heading">Panel heading area</div>
			<div class="panel-body">
				<p>panel body area</p>
			</div>

			<!-- Table -->
			<table class="table table-bordered">
				<thead>
				<tr>
					<th>number</th>
					<th>title</th>
					<th>body</th>
				</tr>
				</thead>
				<tr class="success">
					<td>1</td>
					<td>title1</td>
					<td>body1</td>
				</tr>
				<tr>
					<td>2</td>
					<td>title2</td>
					<td>body2</td>
				</tr>
				<tr>
					<td>3</td>
					<td>title3</td>
					<td>body3</td>
				</tr>
				<tr>
					<td>4</td>
					<td>title4</td>
					<td>body4</td>
				</tr>
			</table>
		</div>
		
		<div class="text-center">
		<ul class="pagination" >
			<li><a href="#" aria-label="Previous"> <span aria-hidden="true">&laquo;</span></a></li>
			<li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li><a href="#">5</a></li>
			<li><a href="#" aria-label="Next"> <span aria-hidden="true">&raquo;</span></a></li>
		</ul>
		</div>
		
	</div>
</body>
</html>
